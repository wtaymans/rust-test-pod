use crate::Type;
use crate::Object;
use crate::Property;
use crate::pod::*;

pub fn choice_filter<T: PartialOrd+Copy>(p1: &ChoiceValues<T>, p2: &ChoiceValues<T>)
        -> Option<ChoiceValues<T>> {
    match (&p1, &p2) {
        (ChoiceValues::One{value:v1}, ChoiceValues::One{value:v2})
            if v1 == v2 =>
                Some(ChoiceValues::One{value:*v1}),
        (ChoiceValues::One{value:v1}, ChoiceValues::Range{default,min,max}) |
        (ChoiceValues::Range{default,min,max}, ChoiceValues::One{value:v1})
            if v1 >= min && v1 <= max =>
                Some(ChoiceValues::One{value:*v1}),
        (ChoiceValues::One{value:v1}, ChoiceValues::Step{default,min,max,step}) |
        (ChoiceValues::Step{default,min,max,step}, ChoiceValues::One{value:v1})
            if v1 >= min && v1 <= max =>
                Some(ChoiceValues::One{value:*v1}),
        (ChoiceValues::One{value:v1}, ChoiceValues::Enum{default,alternatives}) |
        (ChoiceValues::Enum{default,alternatives}, ChoiceValues::One{value:v1})
            if alternatives.contains(v1) =>
                Some(ChoiceValues::One{value:*v1}),

        (ChoiceValues::Range{default:def1,min:min1,max:max1},
         ChoiceValues::Range{default:def2,min:min2,max:max2}) => {
            let min = if *min1 < *min2 { *min2 } else { *min1 };
            let max = if *max1 < *max2 { *max1 } else { *max2 };
            let def = min;
            if min < max {
                Some(ChoiceValues::Range{default:def,min:min,max:max})
            } else if min == max {
                Some(ChoiceValues::One{value:min})
            } else {
                None
            }
        },
        (ChoiceValues::Range{default:rdef,min:rmin,max:rmax},
             ChoiceValues::Step{default:sdef,min:smin,max:smax,step}) |
        (ChoiceValues::Step{default:sdef,min:smin,max:smax,step},
            ChoiceValues::Range{default:rdef,min:rmin,max:rmax}) => {
            let min = if *rmin < *smin { *smin } else { *rmin };
            let max = if *rmax < *smax { *rmax } else { *smax };
            let def = min;
            if min < max {
                Some(ChoiceValues::Step{default:def,min:min,max:max,step:*step})
            } else if min == max {
                Some(ChoiceValues::One{value:min})
            } else {
                None
            }
        },
        (ChoiceValues::Range{default:rdef,min,max},
             ChoiceValues::Enum{default:edef,alternatives}) |
        (ChoiceValues::Enum{default:edef,alternatives},
            ChoiceValues::Range{default:rdef,min,max}) => {
            let mut alt = Vec::new();
            for v in alternatives {
                if v >= min && v <= max {
                    alt.push(*v);
                }
            }
            match alt.len() {
                0 => None,
                1 => Some(ChoiceValues::One{value: alt.remove(0)}),
                _ => Some(ChoiceValues::Enum{default:*edef,alternatives: alt})
            }
        },

        (ChoiceValues::Step{default:def1,min:min1,max:max1,step:step1},
             ChoiceValues::Step{default:def2,min:min2,max:max2,step:step2}) => {
            let min = if *min1 < *min2 { *min2 } else { *min1 };
            let max = if *max1 < *max2 { *max1 } else { *max2 };
            let def = min;
            if min < max {
                Some(ChoiceValues::Step{default:def,min:min,max:max,step:*step1})
            } else if min == max {
                Some(ChoiceValues::One{value:min})
            } else {
                None
            }
        },
        (ChoiceValues::Step{default:rdef,min,max,step},
             ChoiceValues::Enum{default:edef,alternatives}) |
        (ChoiceValues::Enum{default:edef,alternatives},
            ChoiceValues::Step{default:rdef,min,max,step}) => {
            let mut alt: Vec<T> = alternatives.iter().copied()
                .filter(|v| v >= min && v <= max).collect();
            match alt.len() {
                0 => None,
                1 => Some(ChoiceValues::One{value: alt.remove(0)}),
                _ => Some(ChoiceValues::Enum{default:*edef,alternatives: alt})
            }
        },

        (ChoiceValues::Enum{default:def1,alternatives:alt1},
             ChoiceValues::Enum{default:def2,alternatives:alt2}) => {
            let mut alt: Vec<T> = alt1.iter().copied()
                .filter(|v| alt2.contains(v)).collect();
            match alt.len() {
                0 => None,
                1 => Some(ChoiceValues::One{value: alt.remove(0)}),
                _ => Some(ChoiceValues::Enum{default:*def1,alternatives: alt})
            }
        },
        _ => None,
    }
}

pub fn get_choice(p1: &Property) -> Option<ChoiceType> {
    match &p1.value {
        Type::Bool(v1) => Some(ChoiceType::Bool(ChoiceValues::One{ value: *v1 })),
        Type::Int(v1) => Some(ChoiceType::Int(ChoiceValues::One{ value: *v1 })),
        Type::Long(v1) => Some(ChoiceType::Long(ChoiceValues::One{ value: *v1 })),
        Type::Float(v1) => Some(ChoiceType::Float(ChoiceValues::One{ value: *v1 })),
        Type::Double(v1) => Some(ChoiceType::Double(ChoiceValues::One{ value: *v1 })),
        Type::Id(v1) => Some(ChoiceType::Id(ChoiceValues::One{ value: *v1 })),
        Type::Rectangle(v1) => Some(ChoiceType::Rectangle(ChoiceValues::One{ value: *v1 })),
        Type::Fraction(v1) => Some(ChoiceType::Fraction(ChoiceValues::One{ value: *v1 })),
        Type::Choice(v1) => Some(v1.clone()),
        _ => None
    }
}

pub fn filter_property(p1: &Property, p2: &Property) -> Option<Property> {
    let c1 = get_choice(p1)?;
    let c2 = get_choice(p2)?;

    println!("{:?} {:?}", c1, c2);

    let t = match (&c1, &c2) {
        (ChoiceType::Bool(v1), ChoiceType::Bool(v2)) => {
            match choice_filter(v1, v2)? {
                ChoiceValues::One{value} => Type::Bool(value),
                v => Type::Choice(ChoiceType::Bool(v)),
            }
        },
        (ChoiceType::Int(v1), ChoiceType::Int(v2)) => {
            match choice_filter(v1, v2)? {
                ChoiceValues::One{value} => Type::Int(value),
                v => Type::Choice(ChoiceType::Int(v)),
            }
        },
        (ChoiceType::Long(v1), ChoiceType::Long(v2)) => {
            match choice_filter(v1, v2)? {
                ChoiceValues::One{value} => Type::Long(value),
                v => Type::Choice(ChoiceType::Long(v)),
            }
        },
        (ChoiceType::Float(v1), ChoiceType::Float(v2)) => {
            match choice_filter(v1, v2)? {
                ChoiceValues::One{value} => Type::Float(value),
                v => Type::Choice(ChoiceType::Float(v)),
            }
        },
        (ChoiceType::Double(v1), ChoiceType::Double(v2)) => {
            match choice_filter(v1, v2)? {
                ChoiceValues::One{value} => Type::Double(value),
                v => Type::Choice(ChoiceType::Double(v)),
            }
        },
        (ChoiceType::Id(v1), ChoiceType::Id(v2)) => {
            match choice_filter(v1, v2)? {
                ChoiceValues::One{value} => Type::Id(value),
                v => Type::Choice(ChoiceType::Id(v)),
            }
        },
        (ChoiceType::Rectangle(v1), ChoiceType::Rectangle(v2)) => {
            match choice_filter(v1, v2)? {
                ChoiceValues::One{value} => Type::Rectangle(value),
                v => Type::Choice(ChoiceType::Rectangle(v)),
            }
        },
        (ChoiceType::Fraction(v1), ChoiceType::Fraction(v2)) => {
            match choice_filter(v1, v2)? {
                ChoiceValues::One{value} => Type::Fraction(value),
                v => Type::Choice(ChoiceType::Fraction(v)),
            }
        },
        (_, _) => return None
    };
    Some(Property { id: p1.id, flags: p1.flags, value: t })
}

pub fn filter(obj: &Object, filter: Option<&Object>) -> Option<Object> {
    if let Some(filter) = filter {
        if filter.tp != obj.tp {
            return None
        }
        let mut new = Object {
                        tp: filter.tp,
                        id: filter.id,
                        properties: vec!() };

        for pf in &filter.properties {
            match obj.find_property(pf.id) {
                Some(po) =>
                    match filter_property(&po, &pf) {
                        Some(pr) => new.add_property(pr),
                        None => return None
                    },
                None => new.add_property(pf.clone()),
            }
        }
        for po in &obj.properties {
            if filter.find_property(po.id).is_none() {
                new.add_property(po.clone());
            }
        }
        Some(new)
    } else {
        Some(obj.clone())
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_filter_obj() {
        let a = Object { 
            tp: 1,
            id: 2,
            properties: vec!{
                Property { id: 1, flags: 0, value: Type::Int(10) },
                Property { id: 2, flags: 0, value: Type::Bool(false) }
            }
        };
        let b = Object { 
            tp: 1,
            id: 2,
            properties: vec!{
                Property { id: 1, flags: 0, value: Type::Int(10) },
                Property { id: 3, flags: 0, value: Type::Float(1.0) }
            }
        };
        let c = Object { 
            tp: 1,
            id: 2,
            properties: vec!{
                Property { id: 1, flags: 0, value: Type::Int(10) },
                Property { id: 3, flags: 0, value: Type::Float(1.0) },
                Property { id: 2, flags: 0, value: Type::Bool(false) },
            }
        };
        assert_eq!(filter(&a, Option::None), Some(a.clone()));
        assert_eq!(filter(&a, Some(&a)), Some(a.clone()));
        assert_eq!(filter(&a, Some(&b)), Some(c.clone()));
    }

    #[test]
    fn test_filter_prop_int() {
        assert_eq!(filter_property(
                &Property { id: 1, flags: 0, value: Type::Int(1) }, 
                &Property { id: 1, flags: 0, value: Type::Int(2) }), 
                None);
        assert_eq!(filter_property(
                &Property { id: 1, flags: 0, value: Type::Int(1) }, 
                &Property { id: 1, flags: 0, value: Type::Int(1) }), 
                Some(Property { id: 1, flags: 0, value: Type::Int(1) })); 
        assert_eq!(filter_property(
                &Property { id: 1, flags: 0, value: Type::Int(1) }, 
                &Property { id: 1, flags: 0,
                    value: Type::Choice(ChoiceType::Int(ChoiceValues::One{value: 1}))}), 
                Some(Property { id: 1, flags: 0, value: Type::Int(1) })); 
        assert_eq!(filter_property(
                &Property { id: 1, flags: 0, value: Type::Int(2) }, 
                &Property { id: 1, flags: 0,
                    value: Type::Choice(ChoiceType::Int(
                            ChoiceValues::Range{default: 4, min:1, max:10}))}), 
                Some(Property { id: 1, flags: 0, value: Type::Int(2) })); 
        assert_eq!(filter_property(
                &Property { id: 1, flags: 0, value: Type::Int(2) }, 
                &Property { id: 1, flags: 0,
                    value: Type::Choice(ChoiceType::Int(
                            ChoiceValues::Range{default: 5, min:5, max:10}))}), 
                None);
        assert_eq!(filter_property(
                &Property { id: 1, flags: 0, value: Type::Int(20) }, 
                &Property { id: 1, flags: 0,
                    value: Type::Choice(ChoiceType::Int(
                            ChoiceValues::Range{default: 5, min:5, max:10}))}), 
                None);
        assert_eq!(filter_property(
                &Property { id: 1, flags: 0,
                    value: Type::Choice(ChoiceType::Int(
                            ChoiceValues::Range{default: 1, min:1, max:2}))}, 
                &Property { id: 1, flags: 0,
                    value: Type::Choice(ChoiceType::Int(
                            ChoiceValues::Range{default: 4, min:2, max:10}))}), 
                Some(Property { id: 1, flags: 0, value: Type::Int(2) })); 
        assert_eq!(filter_property(
                &Property { id: 1, flags: 0,
                    value: Type::Choice(ChoiceType::Int(
                            ChoiceValues::Range{default: 1, min:3, max:6}))}, 
                &Property { id: 1, flags: 0,
                    value: Type::Choice(ChoiceType::Int(
                            ChoiceValues::Range{default: 4, min:2, max:10}))}), 
                Some(Property { id: 1, flags: 0,
                    value: Type::Choice(ChoiceType::Int(
                            ChoiceValues::Range{default: 3, min:3, max:6}))})); 
        assert_eq!(filter_property(
                &Property { id: 1, flags: 0,
                    value: Type::Choice(ChoiceType::Int(
                            ChoiceValues::Range{default: 1, min:3, max:6}))}, 
                &Property { id: 1, flags: 0,
                    value: Type::Choice(ChoiceType::Int(
                            ChoiceValues::Range{default: 7, min:7, max:10}))}), 
                None);
    }
    fn test_choice_filter() {
    }
}
