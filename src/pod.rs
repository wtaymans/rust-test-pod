#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Rectangle {
    pub width: u32,
    pub height: u32,
}

use core::cmp::Ordering;
use core::cmp::Ordering::*;

impl PartialOrd for Rectangle {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.width == other.width && self.height == other.height {
            Some(Equal)
        } else if self.width < other.width || self.height < other.height {
            Some(Less)
        } else {
            Some(Greater)
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Fraction {
    pub num: u32,
    pub denom: u32,
}

impl PartialOrd for Fraction {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let n1 = self.num as u64 * other.denom as u64;
        let n2 = other.num as u64 * self.denom as u64;
        if n1 == n2 {
            Some(Equal)
        } else if n1 < n2 {
            Some(Less)
        } else {
            Some(Greater)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum ArrayType {
    Bool(Vec<bool>),
    Int(Vec<i32>),
    Long(Vec<i64>),
    Float(Vec<f32>),
    Double(Vec<f64>),
    Id(Vec<u32>),
    Rectangle(Vec<Rectangle>),
    Fraction(Vec<Fraction>),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Type {
    None,
    Bool(bool),
    Int(i32),
    Long(i64),
    Float(f32),
    Double(f64),
    Id(u32),
    Rectangle(Rectangle),
    Fraction(Fraction),
    Choice(ChoiceType),
    Struct(Vec<Type>),
    Array(ArrayType),
    Object(Object),
}

#[derive(Debug, Clone, PartialEq)]
pub enum ChoiceValues<T: PartialOrd> {
    One { value: T },
    Range {
        default: T,
        min: T,
        max: T },
    Step {
        default: T,
        min: T,
        max: T,
        step: T },
    Enum {
        default: T,
        alternatives: Vec<T> },
}

#[derive(Debug, Clone, PartialEq)]
pub enum ChoiceFlags<T> {
    Flags {
        default: T,
        alternatives: Vec<T> },
}

#[derive(Debug, Clone, PartialEq)]
pub enum ChoiceType {
    Bool(ChoiceValues<bool>),
    Int(ChoiceValues<i32>),
    Long(ChoiceValues<i64>),
    Float(ChoiceValues<f32>),
    Double(ChoiceValues<f64>),
    Id(ChoiceValues<u32>),
    Rectangle(ChoiceValues<Rectangle>),
    Fraction(ChoiceValues<Fraction>),
    FlagsInt(ChoiceFlags<u32>),
    FlagsLong(ChoiceFlags<u64>),
}

#[derive(Debug, Clone, PartialEq)]
pub struct Property {
    pub id: u32,
    pub flags: u32,
    pub value: Type
}

#[derive(Debug, Clone, PartialEq)]
pub struct Object {
    pub tp: u32,
    pub id: u32,
    pub properties: Vec<Property>,
}

impl Object {
    pub fn find_property(&self, id: u32) -> Option<&Property> {
        self.properties.iter().find(|&x| x.id == id)
    }
    pub fn add_property(&mut self, property: Property) {
        self.properties.push(property);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_rectangle() {
        let a = Rectangle{width: 100, height: 200};
        let b = Rectangle{width: 200, height: 250};
        assert_eq!(a.partial_cmp(&b), Some(Ordering::Less));
        assert_eq!(b.partial_cmp(&a), Some(Ordering::Greater));
        assert_eq!(a.partial_cmp(&a), Some(Ordering::Equal));
    }

    #[test]
    fn test_fraction() {
        let a = Fraction{num: 10, denom: 200};
        let b = Fraction{num: 20, denom: 250};
        assert_eq!(a.partial_cmp(&b), Some(Ordering::Less));
        assert_eq!(b.partial_cmp(&a), Some(Ordering::Greater));
        assert_eq!(a.partial_cmp(&a), Some(Ordering::Equal));
    }

    #[test]
    fn test_object() {
        let a = Object { 
            tp: 1,
            id: 2,
            properties: vec!{
                Property { id: 1, flags: 0, value: Type::Int(10) },
                Property { id: 2, flags: 0, value: Type::Bool(false) }
            }
        };
        let p = a.find_property(2);
        assert_eq!(p.is_some(), true);
        assert_eq!(match p { Some(Property{..}) => true, _=>false}, true);
        match p {
            Some(Property{id,flags,value}) => {
                assert_eq!(*id, 2); 
                assert_eq!(*flags, 0); 
                assert_eq!(*value, Type::Bool(false)); 
                ()
            },
            _ => ()
        }
    }
}
