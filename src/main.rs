use structopt::StructOpt;

mod pod;
mod filter;
mod json;
mod node;
mod audiosrc;

use crate::pod::*;
use crate::json::JsonValue;
use crate::json::JsonIter;
use crate::node::*;
use crate::audiosrc::*;

#[derive(Debug, StructOpt)]
#[structopt(name = "pipewire", about = "PipeWire daemon")]
struct Opt {
    #[structopt(short = "V", long = "version", help = "Show version")]
    version: bool,
    #[structopt(short = "v", long = "verbose",
                help = "Increase debug", parse(from_occurrences))]
    verbose: u8,
}

fn dump_json_value(v: &mut JsonValue) {
    match v {
        JsonValue::Null => print!("null"),
        JsonValue::Number(i) => print!("{}", i),
        JsonValue::Bool(b) => print!("{}", if *b { "true" } else { "false" }),
        JsonValue::String(s) => print!("\"{}\"", s),
        JsonValue::Array(it) => {
            print!("[");
            let f = dump_json(it);
            print!("{}]", if f { " " } else { "\n" });
        }
        JsonValue::Object(it) => {
            println!("{{");
            let mut f = true;
            while let Some((k,mut v)) = it.next_kv() {
                print!("{}\"{}\": ", if f { "" } else { "\n" }, k);
                dump_json_value(&mut v);
                f = false;
            }
            print!("{}}}", if f { "" } else { "\n" });
        }
    }
}

fn dump_json(iter: &mut JsonIter) -> bool {
    let mut f = true;
    while let Some(mut v) = iter.next() {
        print!("{}", if f { "\n" } else { ", \n" });
        dump_json_value(&mut v);
        f = false;
    }
    f
}

fn main() {
    let opt = Opt::from_args();

    if opt.version {
    }
    let src = AudioSrc::new();
    let obj = src.get_param(Item::InputPort, 0, 0, 0);
    println!("{:?}", obj);

    let s = "{ foo 12.0 baz: [ null \"test 1 2\t 3 \\u1234\"] } test bar ";
    let mut iter = JsonIter::new(s);
    dump_json(&mut iter);

    let s = "{ \"foo\" = bar, ape: 12.0 boo: [ null 5 \"arm is \" \\ \" ] }";
    let mut iter = JsonIter::new(s);

    if let Some(JsonValue::Object(mut i)) = iter.next() {
        while let Some((k,v)) = i.next_kv() {
            println!("{:?} {:?}", k, v);
        }
    }
}
