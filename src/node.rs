use std::collections::HashMap;

use crate::pod::*;
use crate::Object;
use crate::JsonValue;

pub enum Item {
    Node,
    InputStream,
    OutputStream,
    InputPort,
    OutputPort,
}

pub struct Info<'a> {
    pub item: Item,
    pub id: u32,
    pub map: HashMap<&'static str,JsonValue<'a>>
}

pub trait Node {
    fn get_info(&self, item: Item, id: u32) -> Option<Info>;

    fn get_param(&self, item: Item, id: u32, param_id:u32, index: u32) -> Option<Object>;
    fn set_param(&self, item: Item, id: u32, param_id:u32, obj: Option<Object>) -> bool;

    fn add_item(&self, info: Info) -> bool { false }
    fn del_item(&self, item: Item, id: u32) -> bool { false }

    fn set_io(&self, item: Item, id: u32, io_id:u8, mem: &mut [u8]) -> bool;

    fn process(&self) -> bool;
}
