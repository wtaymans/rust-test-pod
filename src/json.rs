use std::str::CharIndices;
use std::iter::Peekable;
use std::str::FromStr;

#[derive(Debug)]
pub enum JsonValue<'a>
{
    Null,
    Number(f64),
    Bool(bool),
    String(String),
    Array(JsonIter<'a>),
    Object(JsonIter<'a>),
}

pub type JsonResult<'a> = Option<JsonValue<'a>>;
pub type JsonResultKV<'a> = Option<(String,JsonValue<'a>)>;

#[derive(Debug)]
pub struct JsonIter<'a>
{
    chars: &'a str,
    it: Peekable<CharIndices<'a>>,
    depth: i32,
}


impl<'a> JsonIter<'a> {
    pub fn new(c: &'a str) -> Self {
        JsonIter {
            chars: c,
            it: c.char_indices().peekable(),
            depth: 0
        }
    }
    fn is_whitespace(&self, c: char) -> bool {
        match c {
            '\u{0020}' | '\u{000a}' | '\u{000d}' | '\u{0009}' |
            ':' | '=' | ',' => true,
            _ => false,
        }
    }
    fn collect_bare(&mut self) -> Option<String> {
        let mut s = String::new();
        loop {
            let (_pos, c) = self.it.peek().copied()?;
            if self.is_whitespace(c) {
                break;
            }
            s.push(c);
            self.it.next()?;
        }
        return Some(s);
    }
    fn next_start(&mut self) -> Option<char> {
        loop {
            let (_pos, c) = self.it.peek().copied()?;
            if !self.is_whitespace(c) {
                return Some(c);
            }
            self.it.next()?;
        }
    }

    pub fn parse_number(&mut self) -> JsonResult {
        let s = self.collect_bare()?;
        let n = f64::from_str(s.as_str()).ok()?;
        Some(JsonValue::Number(n))
    }
    pub fn parse_utf16(&mut self, s: &mut Vec<u16>) {
        let mut u = 0u16;
        for _ in 0..4 {
            match self.it.next() {
                Some((_pos,c)) => {
                    if let Some(h) = c.to_digit(16) {
                        u = u * 0x10 + h as u16;
                    }
                }
                None => return
            }
        }
        s.push(u);
    }
    pub fn parse_string(&mut self) -> JsonResult {
        let mut s = String::new();
        let mut utf16 = Vec::<u16>::new();
        self.it.next()?;
        loop {
            let (_pos, c) = self.it.next()?;
            if c == '\\' {
                let (_pos, c) = self.it.next()?;
                match c {
                    '\\' => '\\',
                    'b' => '\u{0008}',
                    'f' => '\u{000c}',
                    'n' => '\n',
                    'r' => '\r',
                    't' => '\t',
                    'u' => {
                        self.parse_utf16(&mut utf16);
                        continue;
                    },
                    _ => c,
                };
            }
            if !utf16.is_empty() {
                if let Ok(utf8) = String::from_utf16(&utf16) {
                    s.push_str(&utf8);
                }
                utf16.clear();
            }
            if c == '"' {
                break;
            }
            s.push(c);
        }
        Some(JsonValue::String(s))
    }
    pub fn parse_container(&mut self) -> JsonResult {
        let (pos, o) = self.it.next()?;
        self.depth += 1;
        let i = JsonIter::new(self.chars.get(pos+1..)?);
        match o {
            '{' => Some(JsonValue::Object(i)),
            '[' => Some(JsonValue::Array(i)),
            _ => None,
        }
    }
    pub fn parse_bare(&mut self) -> JsonResult {
        match self.collect_bare()?.as_str() {
            "null" => Some(JsonValue::Null),
            "true" => Some(JsonValue::Bool(true)),
            "false" => Some(JsonValue::Bool(false)),
            s @ _ => Some(JsonValue::String(String::from(s))),
        }
    }
    fn skip(&mut self) {
        while self.depth > 0 {
            match self.next_token() {
              Some(_) => continue,
              None => break,
            }
        }
    }
    fn next_token(&mut self) -> JsonResult {
        match self.next_start()? {
            '0'..='9' | '-' => self.parse_number(),
            '"' => self.parse_string(),
            '[' | '{' => self.parse_container(),
            ']' | '}' => {
                self.it.next()?;
                self.depth -= 1;
                if self.depth <= 0 {
                    return None;
                }
                self.next_token()
            }
            _ => self.parse_bare(),
        }
    }
    pub fn next(&mut self) -> JsonResult {
        self.skip();
        self.next_token()
    }
    pub fn next_kv(&mut self) -> JsonResultKV {
        match self.next() {
            Some(JsonValue::String(k)) => {
                match self.next_token() {
                    Some(v) => Some((k,v)),
                    None => None,
                }
            },
            _ => None,
        }
    }
}
