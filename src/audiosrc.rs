use std::collections::HashMap;

use crate::node::*;
use crate::pod::*;
use crate::json::*;

pub struct AudioSrc {
    freq: f32,
    phase: f32
}

impl AudioSrc {
    pub fn new() -> Self {
        AudioSrc { freq: 440.0, phase: 0.0 }
    }
}

impl Node for AudioSrc {
    fn get_info(&self, item: Item, id: u32) -> Option<Info> {
        match (item, id) {
            (Item::Node, 0) => 
                Some(Info {
                    item: Item::Node, id: 0,
                    map: HashMap::from(
                        [ ("node.items", JsonValue::Array(JsonIter::new("foo bar"))) ]
                        )
                }),
            (Item::InputPort, 0) => 
                Some(Info {
                    item: Item::InputPort, id: 0,
                    map: HashMap::new()
                }),
            _ => None
        }
    }

    fn get_param(&self, item: Item, id: u32, param_id:u32, index: u32) -> Option<Object> {
        match (item, id) {
            (Item::InputPort, 0) =>
                Some(Object {
                    tp: 1,
                    id: 2,
                    properties: vec! {
                        Property {
                            id: 1, flags: 0,
                            value: Type::None },
                        Property {
                            id: 2, flags: 0,
                            value: Type::Choice(ChoiceType::Int(ChoiceValues::Range{default:44100,min:1,max:1000000})) },
                        Property {
                            id: 3, flags: 0,
                            value: Type::Choice(ChoiceType::Int(ChoiceValues::Range{default:2,min:1,max:64})) }
                    }
                }),
            _ => None
        }
    }
    fn set_param(&self, item: Item, id: u32, param_id:u32, obj: Option<Object>) -> bool {
        match (item, id) {
            (Item::InputPort, 0) => true,
            _ => false,
        }
    }

    fn set_io(&self, item: Item, id: u32, io_id:u8, mem: &mut [u8]) -> bool {
        true
    }

    fn process(&self) -> bool {
        true
    }

}
